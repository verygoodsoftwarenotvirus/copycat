module gitlab.com/verygoodsoftwarenotvirus/copycat

go 1.13

require (
	github.com/dave/jennifer v1.3.0
	github.com/fatih/structtag v1.0.0
)
