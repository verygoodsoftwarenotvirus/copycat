package main

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"strings"

	"github.com/dave/jennifer/jen"
	"github.com/fatih/structtag"
)

func parseFileToJennifer(filename string) (string, error) {
	f, err := parser.ParseFile(token.NewFileSet(), filename, nil, parser.AllErrors)
	if err != nil {
		log.Fatal(err)
	}

	var b bytes.Buffer
	b.WriteString(fmt.Sprintf("jen.NewFile(%s)\n", f.Name.Name))

	ast.Inspect(f, func(n ast.Node) bool {
		if n == nil {
			return true
		}

		switch x := n.(type) {
		case *ast.GenDecl:
			switch x.Tok {
			case token.IMPORT:
				for _, spec := range x.Specs {
					if y, ok := spec.(*ast.ImportSpec); ok {
						cleanedPath := strings.ReplaceAll(y.Path.Value, `"`, ``)
						if y.Name != nil {
							b.WriteString(fmt.Sprintf("nf.ImportAlias(\"%s\", \"%s\")\n", cleanedPath, y.Name.Name))
						} else {
							b.WriteString(fmt.Sprintf("nf.ImportName(\"%s\", \"\")\n", cleanedPath))
						}
					}
				}
			}
		case *ast.FuncDecl:
			b.WriteString(fmt.Sprintf("nf.Func().Id(%s).Params().BlockFunc(func(g *jen.Group) {\n", x.Name.Name))
			for _, stmt := range x.Body.List {
				switch y := stmt.(type) {
				case *ast.ExprStmt:
					switch z := y.X.(type) {
					case *ast.CallExpr:
						switch ft := z.Fun.(type) {
						case *ast.SelectorExpr:
							if n, ok := ft.X.(*ast.Ident); ok {
								b.WriteString(fmt.Sprintf("\tg.Qual(\"%s\", \"%s\").Call()\n", n.Name, ft.Sel.Name))
							}
						default:
							print(ft)
						}
					default:
						print(z)
					}
				default:
					print(y)
				}
			}
			b.WriteString("})\n")
		default:
			print(x)
		}

		return true
	})

	return b.String(), nil
}

func parseFile(filename string) (string, error) {
	f, err := parser.ParseFile(token.NewFileSet(), filename, nil, parser.AllErrors)
	if err != nil {
		log.Fatal(err)
	}

	nf := jen.NewFile(f.Name.Name)

	ast.Inspect(f, func(n ast.Node) bool {
		if n == nil {
			return true
		}

		switch x := n.(type) {
		case *ast.GenDecl:
			switch x.Tok {
			case token.IMPORT:
				for _, spec := range x.Specs {
					if y, ok := spec.(*ast.ImportSpec); ok {
						if y.Name != nil {
							cleanedPath := strings.ReplaceAll(y.Path.Value, `"`, ``)
							nf.ImportAlias(cleanedPath, y.Name.Name)
						} else {
							cleanedPath := strings.ReplaceAll(y.Path.Value, `"`, ``)
							nf.ImportName(cleanedPath, "")
						}
					}
				}
			case token.CONST:
				constants := make([]jen.Code, len(x.Specs))
				for _, spec := range x.Specs {
					switch y := spec.(type) {
					case *ast.ValueSpec:
						if len(y.Names) > 0 && len(y.Values) > 0 {
							if z, ok := y.Values[0].(*ast.BasicLit); ok {
								constant := jen.
									Id(y.Names[0].Name).
									Op("=").
									Lit(strings.ReplaceAll(z.Value, `"`, ``))
								constants = append(constants, constant)
							}
						}
					default:
						print(y)
					}
				}

				nf.Const().Defs(constants...)
			case token.VAR:
				vars := make([]jen.Code, len(x.Specs))
				for _, spec := range x.Specs {

					switch y := spec.(type) {
					case *ast.ValueSpec:
						if len(y.Names) > 0 && len(y.Values) > 0 {
							if z, ok := y.Values[0].(*ast.BasicLit); ok {
								v := jen.
									Id(y.Names[0].Name).
									Op("=").
									Lit(strings.ReplaceAll(z.Value, `"`, ``))
								vars = append(vars, v)
							}
						}
					default:
						print(y)
					}
				}

				nf.Var().Defs(vars...)
			case token.TYPE:
				types := make([]jen.Code, len(x.Specs))

				for _, spec := range x.Specs {
					switch y := spec.(type) {
					case *ast.TypeSpec:
						switch z := y.Type.(type) {
						case *ast.StructType:
							fields := make([]jen.Code, len(z.Fields.List))
							for _, field := range z.Fields.List {
								switch t := field.Type.(type) {
								case *ast.Ident:
									switch t.Name {
									case "string":
										f := jen.Id(field.Names[0].Name).String()
										tags := make(map[string]string)
										if field.Tag != nil {
											ft, err := structtag.Parse(strings.ReplaceAll(field.Tag.Value, "`", ""))
											if err != nil {
												//
											}

											if ft != nil {
												for _, k := range ft.Keys() {
													t, err := ft.Get(k)
													if err != nil {
														//
													}

													if len(t.Options) > 0 {
														tags[t.Key] = fmt.Sprintf("%s,%s", t.Name, strings.Join(t.Options, ","))
													} else {
														tags[t.Key] = t.Name
													}
												}
											}

											f.Tag(tags)
										}
										fields = append(fields, f)
									default:
										print(t.Name)
									}
								default:
									print()
								}
							}

							tt := jen.Id(y.Name.Name).Struct(fields...)
							types = append(types, tt)
						default:
							print(z)
						}

					default:
						print(y)
					}
				}

				nf.Type().Defs(types...)
			default:
				print(x)
			}
		case *ast.FuncDecl:
			nf.Func().Id(x.Name.Name).Params().BlockFunc(func(g *jen.Group) {
				for _, stmt := range x.Body.List {
					switch y := stmt.(type) {
					case *ast.ExprStmt:
						switch z := y.X.(type) {
						case *ast.CallExpr:
							switch ft := z.Fun.(type) {
							case *ast.SelectorExpr:
								if n, ok := ft.X.(*ast.Ident); ok {
									g.Qual(n.Name, ft.Sel.Name)
								}
							default:
								print(ft)
							}
						default:
							print(z)
						}
					default:
						print(y)
					}
				}
			})
		default:
			print(x)
		}

		return true
	})

	var b bytes.Buffer
	nf.Render(&b)

	return b.String(), nil
}

func main() {
	val, err := parseFile("test_fixtures/example_code/simple/hello_world.go")
	if err != nil {
		log.Fatal(err)
	}

	print(val)
}
