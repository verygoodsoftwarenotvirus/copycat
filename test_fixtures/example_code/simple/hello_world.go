package main

import (
	f "fmt"
	"log"
)

const (
	phrase = "hello world!"
)

var (
	vphrase = "goodbye, world!"
)

type (
	thing struct {
		stuff string `json:"stuff,omitempty" xml:"-"`
	}
)

func main() {
	f.Println(phrase)
	log.Println()
}
